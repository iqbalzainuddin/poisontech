

function validateReg() {
    var name = document.getElementById("name").value;
    var password = document.getElementById("password").value;
    var confirm = document.getElementById("confirm").value;

    if (name == "" || password == "" || confirm == "") {
        document.getElementById("status").value = "Please Fill in All Field.";
        return false;
    } else {
        if (/\s/.test(name)) {
            document.getElementById("status").value = "Username cannot have whitespaces.";
            return false;
        } else {
            if (password != confirm) {
                document.getElementById("status").value = "Password does not match.";
                return false;
            } else {
                if (password.length < 8) {
                    document.getElementById("status").value = "Password must be more than 8 char.";
                    return false;
                } else {
                    document.getElementById("status").value = "Processing your request....";
                    sleep(3);
                    return true;
                }
            }
        }
    }
}

function validateLogin() {
    var name = document.getElementById("name").value;
    var password = document.getElementById("password").value;

    if (name == "" || password == "") {
        document.getElementById("status").value = "Please Fill in All Field.";
        return false;
    } else {
        if (/\s/.test(name)) {
            document.getElementById("status").value = "Username cannot have whitespaces.";
            return false;
        } else {
            document.getElementById("status").value = "Processing your request....";
            sleep(3);
            return true;
        }
    }
}

function changespec() {
    document.getElementById("submit").disabled = true;
}

function calculatePrice() {
    var cpu = document.getElementById("cpu").value;
    var gpu = document.getElementById("gpu").value;
    var cooling = document.getElementById("cooling").value;
    var ram = document.getElementById("ram").value;
    var storage = document.getElementById("storage").value;
    var price = 1400;

    if (cpu == "AMD Ryzen 9") {
        price += 2000;
    } else {
        price += 1900;
    }

    if (gpu == "Gigabyte Aorus RTX2070 super 8GB GDDR6") {
        price += 2300;
    } else {
        price += 3200;
    }
     
    if (cooling == "Custom Water Cooling-Dual loop") {
        price += 600;
    } else {
        price += 450;
    }
     
    if (ram == "Crucial Ballistix Sport 32GB DDR4 Performance RAM 2666MHz - 4 X 8GB") {
        price += 500;
    } else {
        price += 1000;
    }
    
    if (storage == "2TB Samsung M.2 PCIe NVME Solid State Drive") {
        price += 1900;
    } else if (storage == "4TB Seagate Barracuda 7200rpm Hard Disk Drive") {
        price += 400;
    } else {
        price += 1000;
    }

    document.getElementById("total").value = "Total Price : RM " + price;
    document.getElementById("price").value = price;

    document.getElementById("bcpu").value = cpu;
    document.getElementById("bgpu").value = gpu;
    document.getElementById("bcool").value = cooling;
    document.getElementById("bram").value = ram;
    document.getElementById("bstorage").value = storage;

    document.getElementById("submit").disabled = false;
    document.getElementById("calculate").disabled = true;

    return false;
}

function validateBooking() {
    var name = document.getElementById("cname").value;

    if (name == "") {
        alert("Enter customer's full name");
        return false;
    } else {
        if (!/\s/.test(name)) {
            alert("Please make sure you enter customer's full name");
            return false;
        } else {
            return true;
        }
    }
}

function validatePay(status) {
    var sta = status;

    if (sta == "YES") {
        alert("This booking has been paid!");
        return false;
    } else {
        return confirm("Settle this payment?");
    }
}