<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link rel="shortcut icon" href="../poisontech/img/logo1.ico" type="image/x-icon">
    <link rel="stylesheet" href="../poisontech/node_modules/bulma/css/bulma.css" type="text/css">
    <link rel="stylesheet" href="../poisontech/node_modules/@fortawesome/css/all.css">

    <script src="js/app.js"></script>
</head>
<body>
    <section class="hero is-dark is-fullheight">
        <div class="hero-body">
            <div class="container">
                <div class="columns">
                    <div class="column is-6">
                        <div style="height: 100%" class="box">
                            <figure>
                                <img style="width:100%" src="img/img/logo2.jpeg" alt="">
                            </figure>
                        </div>
                    </div>
                    <div class="column is-6">
                        <div style="height: 100%" class="box has-text-centered">
                            <form style="padding: 80px" onsubmit="return validateLogin()" action="serverside/login.php" method="post">
                                <h1 style="font-size: 150%; font-family: Courier New, Courier, Lucida Sans Typewriter, Lucida Typewriter, monospace; font-size: 35px; font-style: normal; font-variant: normal; font-weight: 700; line-height: 25px;">POISON TECH CUSTOM PC</h1>
                                <input class="input is-static has-text-centered" type="text" name="status" id="status" value="Login your account." readonly>
                                <br>
                                <input class="input is-rounded" type="text" name="name" id="name" placeholder="Username">
                                <br><br>
                                <input class="input is-rounded" type="password" name="password" id="password" placeholder="Password">
                                <br><br>
                                <button style="width: 100%" class="button is-dark" name="submit" id="submit" type="submit">Login</button>
                                <br><br>
                                <a href="register.php"><u>Register Here</u></a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
</html>