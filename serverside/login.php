<?php
    session_start();
    include 'server.php';

    if (isset($_POST['submit'])) {
        $name = mysqli_real_escape_string($conn, $_POST['name']);
        $pass = mysqli_real_escape_string($conn, $_POST['password']);

        $sql = "SELECT * FROM users WHERE user_name='$name'";
		$result = mysqli_query($conn, $sql);
		$resultCheck = mysqli_num_rows($result);
		if($resultCheck < 1){
			echo "<script type='text/javascript'>alert('Username not exist.'); window.location.href='/poisontech/index.php';</script>";
			exit();
		} else {
            if ($row = mysqli_fetch_assoc($result)) {
                $hashedPwdCheck = password_verify($pass, $row['user_password']);

                if (!$hashedPwdCheck) {
                    echo "<script type='text/javascript'>alert('Wrong Password.'); window.location.href='/poisontech/index.php';</script>";
			        exit();
                } else {
                    $_SESSION['id'] = $row['user_id'];
					$_SESSION['username'] = $row['user_name'];
					$_SESSION['password'] = $row['user_password'];
					echo "<script type='text/javascript'>window.location.href='/poisontech/dashboard.php';</script>";
			        exit();
                }
                
            }
        }
    } else {
        echo "<script type='text/javascript'>alert('Input is empty.'); window.location.href='/poisontech/index.php';</script>";
        // header('Location: ../index.php');
        exit();
    }
    