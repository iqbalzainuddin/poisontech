<?php
    session_start();
    include 'server.php';
    // var_dump($_POST['cpu']);
    if (isset($_POST['submit'])) {
        $name = mysqli_real_escape_string($conn, $_POST['cname']);
        $cpu = mysqli_real_escape_string($conn, $_POST['cpu']);
        $gpu = mysqli_real_escape_string($conn, $_POST['gpu']);
        $cooling = mysqli_real_escape_string($conn, $_POST['cooling']);
        $ram = mysqli_real_escape_string($conn, $_POST['ram']);
        $storage = mysqli_real_escape_string($conn, $_POST['storage']);
        $price = mysqli_real_escape_string($conn, $_POST['price']);
        $staff = $_SESSION['id'];
        // var_dump($cpu);
        $sql = "INSERT INTO `bookings` (`booking_name`,`cpu`,`gpu`,`cooling`,`ram`,`storage`,`price`,`user_id`) VALUES ('$name', '$cpu', '$gpu', '$cooling', '$ram', '$storage', '$price', '$staff')";
        $results = mysqli_query($conn, $sql);
    
        if($results){
            mysqli_commit();
            echo "<script type='text/javascript'>alert('PC Booking Successful.'); window.location.href='/poisontech/newbooking.php';</script>";
            exit();
        } else {
            mysqli_rollback();
            echo "<script type='text/javascript'>alert('Internal Error.'); window.location.href='/poisontech/newbooking.php';</script>";
            exit();
        }
    }
    else {
        echo "<script type='text/javascript'>alert('Input is empty.'); window.location.href='/poisontech/newbooking.php';</script>";
        exit();
    }