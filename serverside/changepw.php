<?php
    session_start();
    include 'server.php';

    $current = mysqli_real_escape_string($conn, $_POST['current']);
    $new = mysqli_real_escape_string($conn, $_POST['new']);
    $newpw = password_hash($new, PASSWORD_DEFAULT);

    if (!password_verify($current, $_SESSION['password'])) {
        echo "<script type='text/javascript'>alert('Wrong current password.'); window.location.href='/poisontech/changepw.php';</script>";
        exit();
    } else {
        $sql = "UPDATE users SET user_password='$newpw'";
        $result = mysqli_query($conn, $sql);

        if ($result) {
            mysqli_commit();
            echo "<script type='text/javascript'>alert('Password has been changed.'); window.location.href='/poisontech/dashboard.php';</script>";
            exit();
        } else {
            mysqli_rollback();
            echo "<script type='text/javascript'>alert('Internal error.'); window.location.href='/poisontech/changepw.php';</script>";
            exit();
        }
    }
    