<?php
    include 'server.php';

    $id = $_GET['id'];

    $sql = "SELECT * FROM bookings WHERE booking_id='$id'";
    $result = mysqli_query($conn, $sql);
    $resultCheck = mysqli_num_rows($result);

    if ($resultCheck < 1) {
        echo "<script type='text/javascript'>alert('Booking not exist.'); window.location.href='/poisontech/details.php?id=$id';</script>";
        exit();
    } else {
        $sql = "DELETE FROM bookings where booking_id='$id'";
        $result = mysqli_query($conn, $sql);

        if ($result) {
            mysqli_commit();
            echo "<script type='text/javascript'>alert('Booking has been deleted.'); window.location.href='/poisontech/manage.php';</script>";
            exit();
        } else {
            mysqli_rollback();
            echo "<script type='text/javascript'>alert('Internal Error.'); window.location.href='/poisontech/details.php?id=$id';</script>";
            exit();
        }
        
    }
    
