<?php
    include 'server.php';

    if (isset($_POST['submit'])) {
        $name = mysqli_real_escape_string($conn, $_POST['name']);
        $pass = mysqli_real_escape_string($conn, $_POST['password']);
        $password = password_hash($pass, PASSWORD_DEFAULT);

        $sql = "INSERT INTO `users` (`user_name`,`user_password`) VALUES ('$name', '$password')";
        $results = mysqli_query($conn, $sql);

        if($results){
            mysqli_commit();
            echo "<script type='text/javascript'>alert('Registration Successful.'); window.location.href='/poisontech/index.php';</script>";
            // header('Location: ../index.php');
            exit();
        } else {
            mysqli_rollback();
            echo "<script type='text/javascript'>alert('Internal Error.'); window.location.href='/poisontech/register.php';</script>";
            // header('Location: ../register.php');
            exit();
        }
    } else {
        echo "<script type='text/javascript'>alert('Input is empty.'); window.location.href='/poisontech/register.php';</script>";
        // header('Location: ../register.php');
        exit();
    }
    