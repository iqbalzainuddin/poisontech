<?php
    include_once 'layouts/header.php';
?>

<section style="background: url(../poisontech/img/img/main1.jpg) center no-repeat; background-size: cover" class="hero is-dark is-fullheight">
    <div class="hero-body">
        <div class="container has-text-centered">
            <div class="columns">
                <div class="column is-4"></div>
                <div class="column is-4">
                    <div class="box">
                        <h1 class="title has-text-dark">
                            <b>Change Password</b>
                        </h1>
                        <h2 class="has-text-dark">
                            <b>Make sure your password has at least 8 character.</b>
                        </h2>
                        <br>
                        <form action="serverside/changepw.php" method="post" onsubmit="return validateChangePW()">
                            <input class="input is-rounded" type="password" name="current" id="current" placeholder="Current Password">
                            <br><br>
                            <input class="input is-rounded" type="password" name="new" id="new" placeholder="New Password">
                            <br><br>
                            <button class="button is-info" name="submit" id="submit" type="submit" value="submit">Change Password</button>
                        </form>
                    </div>
                </div>
                <div class="column is-4"></div>
            </div>
            <br>
        </div>
    </div>
</section>

<?php
    include_once 'layouts/footer.php';
?>