-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 18, 2019 at 04:47 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `poisontechcpc`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `booking_id` bigint(5) UNSIGNED NOT NULL,
  `booking_name` varchar(255) NOT NULL,
  `cpu` varchar(255) NOT NULL,
  `gpu` varchar(255) NOT NULL,
  `cooling` varchar(255) NOT NULL,
  `ram` varchar(255) NOT NULL,
  `storage` varchar(255) NOT NULL,
  `price` decimal(10,2) UNSIGNED NOT NULL,
  `payment` varchar(3) NOT NULL DEFAULT 'NO',
  `user_id` bigint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`booking_id`, `booking_name`, `cpu`, `gpu`, `cooling`, `ram`, `storage`, `price`, `payment`, `user_id`) VALUES
(3, 'Ikin Daud', 'AMD Ryzen 9', 'Gigabyte Aorus RTX2070 super 8GB GDDR6', 'Custom Water Cooling-Dual loop', 'Crucial Ballistix Sport 32GB DDR4 Performance RAM 2666MHz - 4 X 8GB', '2TB Samsung M.2 PCIe NVME Solid State Drive', '8700.00', 'YES', 4),
(4, 'Dayah Mohammad', 'AMD Ryzen 9', 'Gigabyte Aorus RTX2070 super 8GB GDDR6', 'NZXT Kraken X52 240 AIO Water Cooling', 'Crucial Ballistix Sport 32GB DDR4 Performance RAM 2666MHz - 4 X 8GB', '2TB MX500 SATA Solid State Drive', '7650.00', 'YES', 4),
(5, 'Dzafri Nazim', 'Intel Core i9 9th Gen', 'Zotac RTX2080 Super AMP COre RGB Edition', 'Custom Water Cooling-Dual loop', 'G.Skill Trident Z RGB 64GB DDR4 Performance RAM 3200MHz (OC) - 4 X 16GB', '2TB MX500 SATA Solid State Drive', '9100.00', 'NO', 4),
(6, 'Idham Bin Mazlan', 'Intel Core i9 9th Gen', 'Zotac RTX2080 Super AMP COre RGB Edition', 'Custom Water Cooling-Dual loop', 'G.Skill Trident Z RGB 64GB DDR4 Performance RAM 3200MHz (OC) - 4 X 16GB', '2TB Samsung M.2 PCIe NVME Solid State Drive', '10000.00', 'NO', 4),
(7, 'Nur Dahlia Afifa Binti Mohamad Zainuddin', 'Intel Core i9 9th Gen', 'Zotac RTX2080 Super AMP COre RGB Edition', 'Custom Water Cooling-Dual loop', 'G.Skill Trident Z RGB 64GB DDR4 Performance RAM 3200MHz (OC) - 4 X 16GB', '2TB Samsung M.2 PCIe NVME Solid State Drive', '10000.00', 'YES', 4);

-- --------------------------------------------------------

--
-- Table structure for table `receipts`
--

CREATE TABLE `receipts` (
  `receipt_id` bigint(5) UNSIGNED NOT NULL,
  `receipt_date` varchar(20) NOT NULL,
  `booking_id` bigint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `receipts`
--

INSERT INTO `receipts` (`receipt_id`, `receipt_date`, `booking_id`) VALUES
(2, '2019-12-17', 3),
(3, '2019-12-17', 4),
(4, '2019-12-18', 7);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` bigint(5) UNSIGNED NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_password`) VALUES
(4, 'Iqbal', '$2y$10$TGdxfZrpg.rnyA/C8JBfoeDBTkz.P6MihEUP.VXkRTs94tPkX16pe'),
(9, 'Admin', '$2y$10$u8iVKSXxRv.pqNx0r4kM4OBStQtlhDfMqKG4PJHTcOutARC/0E3JW');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`booking_id`),
  ADD KEY `one` (`user_id`);

--
-- Indexes for table `receipts`
--
ALTER TABLE `receipts`
  ADD PRIMARY KEY (`receipt_id`),
  ADD KEY `three` (`booking_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `booking_id` bigint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `receipts`
--
ALTER TABLE `receipts`
  MODIFY `receipt_id` bigint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` bigint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bookings`
--
ALTER TABLE `bookings`
  ADD CONSTRAINT `one` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `receipts`
--
ALTER TABLE `receipts`
  ADD CONSTRAINT `three` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`booking_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
