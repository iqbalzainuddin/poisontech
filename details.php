<?php
    include_once 'layouts/header.php';
?>

<section class="hero is-dark is-fullheight">
    <div style="background: url(img/det-bg.jpg) no-repeat center;background-size: cover;" class="hero-body">
        <div class="container">
            <div class="columns">
                <div class="column is-2"></div>
                <div class="column is-8">
                    <h1 class="title has-text-centered">
                        Booking Details
                    </h1>
                    <h2 class="subtitle has-text-centered">
                        Settle Payment - Delete Record
                    </h2>
                    <table class="table is-fullwidth has-background-dark has-text-white">
                        <?php
                            include 'serverside/server.php';
                            $id = $_GET['id'];
                            $sql = "SELECT * FROM bookings WHERE booking_id='$id'";
                            $result = mysqli_query($conn, $sql);
                            $data = mysqli_fetch_assoc($result);
                            $query = "SELECT * FROM receipts WHERE booking_id='$id'";
                            $results = mysqli_query($conn, $query);
                            $check = mysqli_num_rows($results);
                            $row = mysqli_fetch_assoc($results);
                            echo '
                                <tr>
                                    <td style="width: 30%;"><b>Customer Name</b></td>
                                    <td class="has-text-centered">'.$data['booking_name'].'</td>
                                </tr>
                                <tr>
                                    <td style="width: 30%;"><b>Processor</b></td>
                                    <td class="has-text-centered">'.$data['cpu'].'</td>
                                </tr>
                                <tr>
                                    <td style="width: 30%;"><b>Graphic Card</b></td>
                                    <td class="has-text-centered">'.$data['gpu'].'</td>
                                </tr>
                                <tr>
                                    <td style="width: 30%;"><b>Cooling System</b></td>
                                    <td class="has-text-centered">'.$data['cooling'].'</td>
                                </tr>
                                <tr>
                                    <td style="width: 30%;"><b>RAM</b></td>
                                    <td class="has-text-centered">'.$data['ram'].'</td>
                                </tr>
                                <tr>
                                    <td style="width: 30%;"><b>Storage</b></td>
                                    <td class="has-text-centered">'.$data['storage'].'</td>
                                </tr>
                                <tr>
                                    <td style="width: 30%;"><b>Payment</b></td>
                                    <td class="has-text-centered">'.$data['payment'].'</td>
                                </tr>
                                <tr>
                                    <td style="width: 30%;"><b>Total Price</b></td>
                                    <td class="has-text-centered">RM '.$data['price'].'</td>
                                </tr>
                                
                            ';

                            if ($check > 0) {
                                echo '
                                    <tr>
                                        <td style="width: 30%;"><b>Paid on</b></td>
                                        <td class="has-text-centered">'.$row['receipt_date'].'</td>
                                    </tr>
                                ';
                            }
                        ?>
                        <tr>
                            <td colspan=2 Align="center">
                                <a style="width: 25%;" class="button is-success" role="button" onclick="return validatePay('<?=$data['payment']?>')" href="serverside/pay.php?id=<?=$data['booking_id'];?>">Pay</a>
                                <a style="width: 25%;" class="button is-danger" role="button" onclick="return confirm('Delete this booking?')" href="serverside/delete.php?id=<?=$data['booking_id'];?>">Delete</a>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="column is-2"></div>
            </div>
        </div>
    </div>
</section>

<?php
    include_once 'layouts/footer.php';
?>