<?php
    include_once 'layouts/header.php';
?>

<section style="background: url(../poisontech/img/img/main1.jpg) center no-repeat; background-size: cover" class="hero is-dark is-fullheight">
  <div class="hero-body">
    <div class="container has-text-centered">
      <h1 class="title">
        POISON TECH CUSTOM PC
      </h1>
      <h2 class="subtitle">
        God Among Custom PC Builder
      </h2>
      <br>
    </div>
  </div>
</section>

<section class="hero is-dark">
  <div class="hero-body">
    <div class="container has-text-centered">
      <h1 class="title">
        Desktop Case
      </h1>
    </div>
  </div>
</section>

<section style="background: url(../poisontech/img/crystal460x.jpg) center no-repeat; background-size: cover" class="hero is-success is-fullheight">
  <div class="hero-body">
    <div class="container">
      <h1 Align="right" class="title">
        CORSAIR CRYSTAL 460X RGB COMPACT MID-TOWER ATX CASE
      </h1>
      <h2 Align="right" class="subtitle">
        We are using this case for all of the custom PC we build based on <br> customer chosen specification. This case featuring tempered glass <br> 
        side panel with direct airflow cooling. It has room for rear, top <br> and front radiator placement for water cooling system. 460x RGB <br>
        adds a whole new level of lighting immersion - RGB Capability.
      </h2>
    </div>
  </div>
</section>

<section class="hero is-dark">
  <div class="hero-body">
    <div class="container has-text-centered">
      <h1 class="title">
        Motherboard for Intel Core i9 9th Gen
      </h1>
    </div>
  </div>
</section>

<section style="background: url(../poisontech/img/moborog.jpg) center no-repeat; background-size: cover" class="hero is-success is-fullheight">
  <div class="hero-body">
    <div class="container">
      <h1 class="title has-background-dark">
        ROG STRIX Z390-E GAMING
      </h1>
      <h2 class="subtitle has-background-dark">
        We are using this motherboard edition for those who chose Intel Processor
      </h2>
    </div>
  </div>
</section>

<section class="hero is-dark">
  <div class="hero-body">
    <div class="container has-text-centered">
      <h1 class="title">
        Motherboard for AMD Ryzen 9
      </h1>
    </div>
  </div>
</section>

<section style="background: url(../poisontech/img/mobox570.jpg) center no-repeat; background-size: cover" class="hero is-success is-fullheight">
  <div class="hero-body">
    <div class="container">
      <h1 class="title has-background-dark">
        ROG STRIX X570-E GAMING
      </h1>
      <h2 class="subtitle has-background-dark">
        We are using this motherboard edition for those who chose AMD Ryzen Processor
      </h2>
    </div>
  </div>
</section>

<section class="hero is-dark">
  <div class="hero-body">
    <div class="container has-text-centered">
      <h1 class="title">
        Power Supplier
      </h1>
    </div>
  </div>
</section>

<section style="background: url(../poisontech/img/thermaltake.png) center no-repeat; background-size: cover" class="hero is-success is-medium">
  <div class="hero-body">
    <div class="container">
      <h1 Align="center" class="title">
        THERMALTAKE SMART RGB 700W
      </h1>
      <h2 Align="center" class="subtitle has-background-grey">
        with 10 LED bulb and Ultra Quiet 120MM Fan, featuring capacities from 500W to 700W <br> and 80 Plus Standard certification.
      </h2>
    </div>
  </div>
</section>

<section class="hero is-dark">
  <div class="hero-body">
    <div class="container has-text-centered">
      <h1 class="title">
        Peripherals
      </h1>
    </div>
  </div>
</section>

<section style="background: url(../poisontech/img/rogmonitor.jpg) center no-repeat; background-size: cover" class="hero is-info is-large">
  <div class="hero-body">
    <div class="container">
      <h1 class="title">
          ROG STRIX XG258Q 240HZ MONITOR
      </h1>
    </div>
  </div>
</section>

<section style="background: url(../poisontech/img/peripheral.jpg) center no-repeat; background-size: cover" class="hero is-info is-large">
  <div class="hero-body">
    <div class="container">
      <h1 class="title has-text-dark has-background-light">
          ROG GAMING SERIES PERIPHERALS
      </h1>
    </div>
  </div>
</section>

<footer class="footer">
  <div class="content has-text-centered">
    <p>
      <strong>&copy;Poison Tech Custom PC</strong> by Iqbal, Idham <br>
      Made with &copy;<a href="https://bulma.io">Bulma</a>
    </p>
  </div>
</footer>

<?php
    include_once 'layouts/footer.php';
?>