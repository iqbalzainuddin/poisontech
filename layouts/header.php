<!DOCTYPE html>

<?php
    session_start();
	if(!isset($_SESSION['id'])){
		echo "<script type='text/javascript'>alert('Warning! You are not logged in.'); window.location.href='/poisontech/index.php';</script>";
        exit();
	}
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Poison Tech Custom PC</title>
    <link rel="shortcut icon" href="../poisontech/img/logo1.ico" type="image/x-icon">
    <link rel="stylesheet" href="../poisontech/node_modules/bulma/css/bulma.css" type="text/css">
    <link rel="stylesheet" href="../poisontech/node_modules/@fortawesome/css/all.css">
    <link rel="stylesheet" href="../poisontech/css/style.css">
    <link rel="stylesheet" href="../poisontech/node_modules/sweetalert2/dist/sweetalert2.css">
    
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="js/app.js" type="text/javascript"></script>
    <script src="js/next.js" type="text/javascript"></script>
</head>
<body style="font-family: Courier New, Courier, Lucida Sans Typewriter, Lucida Typewriter, monospace;">
    <nav class="navbar is-black" role="navigation" aria-label="main navigation">
        <div style="padding-left: 100px" class="navbar-brand">
            <a class="navbar-item" href="../poisontech/dashboard.php">
                <img src="../poisontech/img/logo1.ico" height="40" width="40">
                <h4 style="font-size: 150%; font-family: Courier New, Courier, Lucida Sans Typewriter, Lucida Typewriter, monospace; font-size: 30px; font-style: normal; font-variant: normal; font-weight: 700; line-height: 25px;" class="title has-text-white">POISON TECH</h4>
            </a>

            <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
            </a>
        </div>

        <div id="navbarBasicExample" class="navbar-menu">
            <div class="navbar-start">
                
            </div>

            <div style="padding-right: 150px" class="navbar-end">
                <a href="../poisontech/newbooking.php" class="navbar-item">
                    New Booking
                </a>

                <a href="../poisontech/manage.php" class="navbar-item">
                    Manage Booking
                </a>

                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link">
                        My Account
                    </a>

                    <div class="navbar-dropdown">
                        <a href="../poisontech/changepw.php" class="navbar-item">
                            Change Password
                        </a>

                        <a href="../poisontech/serverside/logout.php" class="navbar-item">
                            Log Out
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    