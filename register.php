<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register</title>
    <link rel="shortcut icon" href="../poisontech/img/logo1.ico" type="image/x-icon">
    <link rel="stylesheet" href="../poisontech/node_modules/bulma/css/bulma.css" type="text/css">
    <link rel="stylesheet" href="../poisontech/node_modules/@fortawesome/css/all.css">

    <script src="js/app.js"></script>
</head>
<body>
    <section class="hero is-dark is-fullheight">
        <div class="hero-body">
            <div class="container">
                <div class="columns">
                    <div class="column is-4"></div>
                    <div class="column is-4">
                        <div class="box">
                            <div class="columns">
                                <div class="column is-2"></div>
                                <div class="column is-8">
                                    <figure>
                                        <img style="width: 100%; height: 100%" src="img/img/logo2.jpeg" alt="">
                                    </figure>
                                </div>
                                <div class="column is-2"></div>
                            </div>
                        </div>
                    </div>
                    <div class="column is-4"></div>
                </div>
                <h1 style="font-size: 170%" Align="center">Register Your Staff Account</h1>
                <br>
                <div class="columns">
                    <div class="column is-4"></div>
                    <div class="column is-4">
                        <div class="box has-text-centered">
                            <form onsubmit="return validateReg()" action="serverside/register.php" method="post">
                                <input class="input is-static has-text-centered" type="text" name="status" id="status" value="Fill in your details." readonly><br><br>
                                <input class="input is-rounded" type="text" name="name" id="name" placeholder="Username"><br><br>
                                <input class="input is-rounded" type="password" name="password" id="password" placeholder="Password"><br><br>
                                <input class="input is-rounded" type="password" name="confirm" id="confirm" placeholder="Confirm Password"><br><br>
                                <button class="button is-dark" style="width: 100%" name="submit" id="submit" type="submit">Register</button><br><br>
                                <a href="index.php">Back to Login Page</a>
                            </form>
                        </div>
                    </div>
                    <div class="column is-4"></div>
                </div>
            </div>
        </div>
    </section>
</body>
</html>