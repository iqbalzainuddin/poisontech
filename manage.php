<?php
    include_once 'layouts/header.php';
?>

<section class="hero is-success is-fullheight">
    <div class="hero-body manage-bg">
        <div class="container">
            <div class="columns">
                <div class="column is-1"></div>
                <div class="column is-10">
                    <h1 class="title has-text-centered">
                        Booking Manager
                    </h1>
                    <h2 class="subtitle has-text-centered">
                        View Booking Details
                    </h2>
                    <input class="input is-rounded is-fullwidth" name="search" id="search" onkeyup="filtertable()" type="text" placeholder="Type to search...">
                    <br><br>
                    <table id="list" class="table is-striped is-fullwidth">
                        <thead>
                            <tr>
                                <th class="has-text-centered">Customer Name</th>
                                <th class="has-text-centered">Cashier</th>
                                <th class="has-text-centered">Price</th>
                                <th style="width: 30%;" class="has-text-centered">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                include 'serverside/server.php';
                                $sql = "SELECT * FROM bookings";
                                $results = mysqli_query($conn, $sql);

                                while ($row = mysqli_fetch_assoc($results)) {
                                    $id = $row['user_id'];
                                    $query = "SELECT * FROM users WHERE user_id='$id'";
                                    $res = mysqli_query($conn, $query);
                                    $staff = mysqli_fetch_assoc($res);
                                    echo '
                                        <tr>
                                            <td class="has-text-centered">'.$row['booking_name'].'</td>
                                            <td class="has-text-centered">'.$staff['user_name'].'</td>
                                            <td class="has-text-centered">RM '.$row['price'].'</td>
                                            <td class="has-text-centered">
                                                <a class="button is-info" role="button" href="details.php?id='.$row['booking_id'].'">View</a>
                                            </td>
                                        </tr>
                                    ';
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="column is-1"></div>
            </div>
        </div>
    </div>
</section>

<?php
    include_once 'layouts/footer.php';
?>