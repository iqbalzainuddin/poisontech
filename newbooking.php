<?php
    include_once 'layouts/header.php';
?>

<section style="background: url(../poisontech/img/img/wall4.jpg) center no-repeat; background-size: cover" class="hero is-dark is-fullheight">
    <div class="hero-body">
        <div class="container">
            <h1 Align="center" class="title">
                Booking Details
            </h1>
            <div class="columns">
                <div class="column is-3"></div>
                <div class="column is-6">
                    <form style="width: 100%" action="/poisontech/serverside/addbook.php" method="POST" onsubmit="return validateBooking()">        
                        <input class="input is-rounded" type="text" name="cname" id="cname" placeholder="Customer Full Name">
                        <br><br>
                        <h3 Align="center"><b>Choose the processor for the machine</b></h3>
                        <div class="box">
                            <div class="columns">
                                <div class="column is-6">
                                    <figure style="height: 100%; width: 100%">
                                        <img src="img/amdryzen9.jpg" alt="">
                                    </figure>
                                </div>
                                <div class="column is-6">
                                    <figure style="height: 100%; width: 100%">
                                        <img src="img/inteli9.jpg" alt="">
                                    </figure>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <div class="control">
                                <div class="select is-primary is-fullwidth">
                                    <select name="cpu" id="cpu" onchange="changespec()">
                                        <option value="AMD Ryzen 9">AMD Ryzen 9</option>
                                        <option value="Intel Core i9 9th Gen">Intel Core i9 9th Gen</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <br>
                        <h3 Align="center"><b>Choose the graphic processor for the machine</b></h3>
                        <div class="box">
                            <div class="columns">
                                <div class="column is-6">
                                    <figure style="height: 100%; width: 100%">
                                        <img src="img/gigagpu.jpg" alt="">
                                    </figure>
                                </div>
                                <div class="column is-6">
                                    <figure style="height: 100%; width: 100%">
                                        <img src="img/zotac.jpg" alt="">
                                    </figure>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <div class="control">
                                <div class="select is-primary is-fullwidth">
                                    <select name="gpu" id="gpu" onchange="changespec()">
                                        <option value="Gigabyte Aorus RTX2070 super 8GB GDDR6">Gigabyte Aorus RTX2070 super 8GB GDDR6</option>
                                        <option value="Zotac RTX2080 Super AMP COre RGB Edition">Zotac RTX2080 Super AMP COre RGB Edition</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <br>
                        <h3 Align="center"><b>Choose the cooling system for the machine</b></h3>
                        <div class="box">
                            <div class="columns">
                                <div class="column is-6">
                                    <figure style="height: 100%; width: 100%">
                                        <img style="height: 100%; width: 100%" src="img/dualloop.jpg" alt="">
                                    </figure>
                                </div>
                                <div class="column is-6">
                                    <figure style="height: 100%; width: 100%">
                                        <img style="height: 100%; width: 100%" src="img/kraken.jpeg" alt="">
                                    </figure>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <div class="control">
                                <div class="select is-primary is-fullwidth">
                                    <select name="cooling" id="cooling" onchange="changespec()">
                                        <option value="Custom Water Cooling-Dual loop">Custom Water Cooling-Dual loop</option>
                                        <option value="NZXT Kraken X52 240 AIO Water Cooling">NZXT Kraken X52 240 AIO Water Cooling</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <br>
                        <h3 Align="center"><b>Choose the RAM for the machine</b></h3>
                        <div class="box">
                            <div class="columns">
                                <div class="column is-6">
                                    <figure style="height: 100%; width: 100%">
                                        <img style="height: 100%; width: 100%" src="img/ballistic.jpg" alt="">
                                    </figure>
                                </div>
                                <div class="column is-6">
                                    <figure style="height: 100%; width: 100%">
                                        <img style="height: 100%; width: 100%" src="img/trident.png" alt="">
                                    </figure>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <div class="control">
                                <div class="select is-primary is-fullwidth">
                                    <select name="ram" id="ram" onchange="changespec()">
                                        <option value="Crucial Ballistix Sport 32GB DDR4 Performance RAM 2666MHz - 4 X 8GB">Crucial Ballistix Sport 32GB DDR4 Performance RAM 2666MHz - 4 X 8GB</option>
                                        <option value="G.Skill Trident Z RGB 64GB DDR4 Performance RAM 3200MHz (OC) - 4 X 16GB">G.Skill Trident Z RGB 64GB DDR4 Performance RAM 3200MHz (OC) - 4 X 16GB</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <br>
                        <h3 Align="center"><b>Choose the storage for the machine</b></h3>
                        <div class="box">
                            <div class="columns">
                                <div class="column is-4">
                                    <figure style="height: 100%; width: 100%">
                                        <img style="height: 100%; width: 100%" src="img/samsung.jpg" alt="">
                                    </figure>
                                </div>
                                <div class="column is-4">
                                    <figure style="height: 100%; width: 100%">
                                        <img style="height: 100%; width: 100%" src="img/hdd.jpg" alt="">
                                    </figure>
                                </div>
                                <div class="column is-4">
                                    <figure style="height: 100%; width: 100%">
                                        <img style="height: 100%; width: 100%" src="img/ssd.jpg" alt="">
                                    </figure>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <div class="control">
                                <div class="select is-primary is-fullwidth">
                                    <select name="storage" id="storage" onchange="changespec()">
                                        <option value="2TB Samsung M.2 PCIe NVME Solid State Drive">2TB Samsung M.2 PCIe NVME Solid State Drive</option>
                                        <option value="4TB Seagate Barracuda 7200rpm Hard Disk Drive">4TB Seagate Barracuda 7200rpm Hard Disk Drive</option>
                                        <option value="2TB MX500 SATA Solid State Drive">2TB MX500 SATA Solid State Drive</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="bcpu" id="bcpu">
                        <input type="hidden" name="bgpu" id="bgpu">
                        <input type="hidden" name="bcool" id="bcool">
                        <input type="hidden" name="bram" id="bram">
                        <input type="hidden" name="bstorage" id="bstorage">
                        <input type="hidden" name="price" id="price">
                        <input class="input is-static has-text-centered has-background-black has-text-white" type="text" name="total" id="total" value="Calculate Total Price First." readonly>
                        <br><br>
                        <div class="columns">
                            <div class="column is-2"></div>
                            <div class="column is-4">
                                <a style="width: 100%" role="button" class="button is-primary" name="calculate" id="calculate" onclick="return calculatePrice()">Calculate Price</a>
                            </div>
                            <div class="column is-4">
                                <button style="width: 100%" class="button is-primary" type="submit" name="submit" id="submit" disabled>Submit Booking</button>
                            </div>
                            <div class="column is-2"></div>
                        </div>
                    </form>
                </div>
                <div class="column is-3"></div>
            </div>
        </div>
    </div>
</section>

<?php
    include_once 'layouts/footer.php';
?>